using System;

namespace EventsDelegatesHomeWork
{
    public class FileArgs : EventArgs
    {
        public FileArgs(string fileName)
        {
            FileName = fileName;
        }
        public bool IsFindingFile { get; set; }
        public string FileName { get; set; }
    }
}
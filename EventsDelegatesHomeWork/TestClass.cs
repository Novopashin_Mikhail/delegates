using System;

namespace EventsDelegatesHomeWork
{
    public class TestClass
    {
        public char CharValue { get; set; }
        public int Value { get; set; }

        public TestClass(int x)
        {
            var r = new Random();    
            Value = x;
            CharValue = (char)(x + r.Next(10,100));
        }

        public override string ToString()
        {
            return $"CharValue={CharValue}; Value={Value}";
        }
    }
}
using System;
using System.IO;
using System.Linq;

namespace EventsDelegatesHomeWork
{
    public class DirectoryRunner
    {
        public event EventHandler<FileArgs> FileFound;

        public void RunFiles(string path)
        {
            foreach (var fileName in Directory.GetFiles(path).Select(Path.GetFileName))
            {
                var args = new FileArgs(fileName);
                FileFound?.Invoke(this, args);
                if (args.IsFindingFile)
                {
                    Console.WriteLine($"Искомый файл найден: {args.FileName}");
                    break;
                }
            }
        } 
    }
}
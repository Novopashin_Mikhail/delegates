using System;
using System.Collections.Generic;
using System.Linq;

namespace EventsDelegatesHomeWork
{
    public static class Extension
    {
        public static T GetMax<T>(this IEnumerable<T> self, Func<T, float> func) where T : class
        {
            var valueMax = self.Max(x => func(x));
            var elementMax = self.FirstOrDefault(y => func(y) == valueMax);
            return elementMax;
        }
    }
}
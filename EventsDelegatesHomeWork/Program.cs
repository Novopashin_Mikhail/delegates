﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EventsDelegatesHomeWork
{
    class Program
    {
        static void Main(string[] args)
        {
            //Test 1 task
            var r = new Random();
            var list = new List<TestClass>();
            for (int i = 0; i < 10; i++)
            {
                var number = i + r.Next(2, 300);
                var obj = new TestClass(number);
                list.Add(obj);
                Console.WriteLine(number);
            }
            var element= list.GetMax(x => x.Value);
            Console.WriteLine(element);
            
            //Test 2 task
            var path = @"C:\test\angular-docker";
            var fileClass = new DirectoryRunner();
            var findingFile = "package.json";
            fileClass.FileFound += (sender, fileArgs) =>
            {
                Console.WriteLine(fileArgs.FileName);
                if (fileArgs.FileName == findingFile)
                    fileArgs.IsFindingFile = true;
            };
            fileClass.RunFiles(path);
        }
    }
}